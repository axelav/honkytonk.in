---
title: Guitar Topographies
published_at: 2018-12-04T15:00:00.000Z
---

"Guitar Topographies", a mix by Andrew Pekler, hits a lot of the right spots for
me.

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/525582759&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/pekler" title="Andrew Pekler" target="_blank" style="color: #cccccc; text-decoration: none;">Andrew Pekler</a> · <a href="https://soundcloud.com/pekler/guitar-topographies" title="Guitar Topographies" target="_blank" style="color: #cccccc; text-decoration: none;">Guitar Topographies</a></div>

Tracklisting:

- Philip Tabane - Mgwedi (Moon)
- Chas Smith - After
- Marissa Anderson - Chimes
- Roy Montgomery - Clear Night
- A.C. Marias - Just Talk
- Robert Fripp - 1988
- Acetone - How Sweet I Roamed
- Bill Nelson - The Ritual Echo
- Pete Drake - Steal Away
- Andrew Pekler - Untitled Guitar Music
- Spacemen 3 - Repeater
- Daniel Lanois - Sketches
